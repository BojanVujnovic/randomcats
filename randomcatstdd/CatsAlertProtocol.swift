//
//  CatsAlertProtocol.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 3/2/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import UIKit

//MARK: Cats Alert Protocol
protocol CatsAlertProtocol {
    
    var alertBackgroundImage: UIImageView { get }
    var alertBackground: AlertBackground { get }
}

extension CatsAlertProtocol {
    
    func networkAlert(controller: UIViewController) { }
    func setBackgroundImage(_ controller: UIViewController) -> UIImage { return UIImage()}
    func removeBackgroundImage(_ controller: UIViewController) -> UIImage{ return UIImage()}
    func alertConnectivity(_ controller: UIViewController) { }
}
