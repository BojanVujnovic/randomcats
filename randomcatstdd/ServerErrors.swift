//
//  ServerError.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 3/7/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import UIKit

enum ServerErrors: Int {
    case Internal_Server_Error = 500
    case Service_Unavailable = 503
    case Bad_Gatway = 502
    case Gateway_Timeout = 504
}
