//
//  CatsManager.swift
//  RandomCatsTDD
//
//  Created by Mac on 5/23/16.
//  Copyright © 2016 Boki. All rights reserved.
//

import UIKit

struct CatsManager {
    
    //Properties
    
    var catsArray = [Cat]()
    var catsXMLParser = CatsXMLParser()    
    
    var catsCount: Int { return catsArray.count }
    
    //Functions
    
    mutating func addCat(cat: Cat) {
        if !catsArray.contains({ (catImage:Cat) -> Bool in  return catImage.photo == cat.photo }) {
            catsArray.append(cat) }
    }
    
    mutating func randomizeCats() {
        catsArray.sortInPlace { (cat1:Cat, cat2:Cat) -> Bool in
            return arc4random() < arc4random()
        }
    }
}

