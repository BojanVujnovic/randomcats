//
//  UIColorExtension.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 3/8/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var lightYellow: UIColor {
        return UIColor(red:0.984, green:0.937, blue:0.733, alpha:1.00)
    }
}
