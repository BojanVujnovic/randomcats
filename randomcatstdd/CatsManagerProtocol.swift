//
//  CatsManagerProtocol.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 3/2/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import Foundation

//Cats Manager Protocol
protocol CatsManagerProtocol {
    
    var catsArray: [CatVM] { get set }
    var catsXMLParser: CatsXMLParser { get}
    
}

extension CatsManagerProtocol {
    
    var catsCount: Int {
        return  !catsArray.isEmpty ? catsArray.count : 0
    }
    
    }
