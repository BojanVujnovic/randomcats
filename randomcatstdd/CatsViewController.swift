//
//  CatsViewController.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/23/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit


//MARK: CatsViewController class
class CatsViewController: UIViewController, CatsLoadedDelegate {
    
    //MARK: Properties
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var catsTableView: UITableView!
    
    var catsData: CatsDataSource!
    var catsManager: CatsManager!
    
    var counter: Counter?
    let navigation = Navigation()
    var refreshControl: UIRefreshControl!
    
    var animationContainerView: AnimationProtocol!
    let animationTime = AnimationTime()
    
    //ViewController's methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.tintColor = UIColor.lightYellow
        //Adds RefreshControl to CatsTableView
        RefreshControlBridge.refreshControlTableView(controller: self)
        //Bridge pathern between CatsDataSource and CatsParser class instances,sets CatsData, CatsTableView data source and delegate
        if let manager = self.catsManager {
            self.setDataSource(manager: manager) }
        
        //Bridge between AnimationView and self View and adding target to random button
        AnimationViewBridge.addAnimationView(controller: self, animation: MainAnimationView())
        self.counter = Counter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let _ = self.unhideAnimation(animationContainerView)
    }    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //Animates Cat only the first time
        let _ = self.animateOnce(self.counter)
        //Chooses Alert for the Controller
        self.chooseAlert(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Functions
    
    func reloadTableView() {
        DispatchQueue.main.async { [ unowned self ] in
            self.catsTableView.reloadData()
            self.activityIndicator.stopAnimating() }
    }
    
    //Unhides Animation
    func unhideAnimation(_ animationContainer: AnimationProtocol) -> Bool {
        var animationHidden = true
        if let animationContainer = animationContainer as? UIView {
            animationContainer.isHidden = false
            animationHidden = animationContainer.isHidden
        }
        return animationHidden
    }
    
    //Chooses Alert for the Controller
    func chooseAlert(_ controller: UIViewController)  {
       if let catsAlert = getAlert(controller: controller) as? CatsAlert {
          catsAlert.networkAlert(controller: self)
        }
    }
    
    //Animates Transition of an Image to Button once
    func animateOnce(_ counter: Counter?) -> Bool {
        if let _counter = counter, _counter.count <= Counter.oneTime {
            self.animationContainerView.transitionOfImageToButton(self.animationContainerView.catImageView)
            self.counter?.addCount()
        } else { self.counter = nil}        
        return counter != nil ? (counter!.count == 1 || counter?.count == 2) : counter == nil
    }
    
    //Creates a XML Parser
    func setDataSource(manager: CatsManager){
          manager.apiManager?.parser.loadingDelegate = self
          (manager.apiManager?.parser as? CatsParser)?.catsUpdateDelegate = manager
          self.catsData = CatsDataSource()        
          DataSourceBridge.catsTableDataSourceSettings(manager: manager , data: self.catsData, tableView: self.catsTableView)
        if !self.refreshControl.isRefreshing {
            self.activityIndicator.startAnimating()
        } else { self.activityIndicator.stopAnimating() }
    }
    
    //Refreshes CatsTableView with the latests cats
    func getLatestCats(_ refreshControl: UIRefreshControl) {
        if refreshControl.isRefreshing {
            self.refreshControl.tintColor = UIColor.lightYellow
            self.activityIndicator.stopAnimating()
            let parser = CatsParser()
            let apiManager = RESTApiManager(parser: parser)
            self.catsManager = CatsManager(api: apiManager)
            self.setDataSource(manager: self.catsManager)
        }
        self.refreshControl.endRefreshing()
    }
    
    //Random Cats images
    func randomCats(_ button: UIButton) {
         DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async  {
            self.catsManager.randomizeCats()
            DispatchQueue.main.async { [unowned self] in
               //Animation of the Button
                self.animationContainerView.animateRandomButton(catImage: self.animationContainerView.catImageView!)               
               self.catsTableView.reloadData()
            }
        }
        self.animationContainerView.randomButton.shake()
    }
        
    //Segue to CatsDetailViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let catIdentifier = "toCatDetail"
        if segue.identifier == catIdentifier {
            let _ = self.transferDataBySegue(segue)
        } else { return print("There is no \"\(catIdentifier)\" Segue identifier") }
    }
    
    //Transfers data by the Segue
    fileprivate func transferDataBySegue(_ segue: UIStoryboardSegue) -> (CatVM) {
        let catViewController = segue.destination as! CatDetailViewController
        if let catIndex = self.catsTableView.indexPathForSelectedRow {
            if let cat = self.catsManager[catIndex.row] {
                catViewController.cat = cat }
            if let animationContainerView = self.animationContainerView as? UIView {
                animationContainerView.isHidden = true }
        }
        return catViewController.cat
    }    
}

enum DataSuccess: String {
    case DataSuccessful = "Data have been successfully downloaded."
    case DataFailure = "Data have failed request from URL."
}






