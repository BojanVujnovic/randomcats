//
//  CatAPI.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 3/7/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import Foundation

struct CatAPI {
    
    private static let base_URL = "http://thecatapi.com/api/images/"
    private static let GET_URL = "\(base_URL)get?format=xml&results_per_page="
    static let catsNumber = 50
    static let catsURL = "\(GET_URL)\(catsNumber)"
}
