//
//  RefreshControlBridge.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/15/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit

struct RefreshControlBridge {
    
    //MARK: Functions
    
    static func refreshControlTableView(controller: UIViewController) {
        let refresh = RefreshControl()
        if let catsController = controller as? CatsViewController {
           catsController.refreshControl = refresh.frame
           catsController.refreshControl.backgroundColor = refresh.backgroundColor
           catsController.refreshControl.tintColor = refresh.tintColor
           catsController.refreshControl.addTarget(catsController, action: Selector.getLatestCats, for: .valueChanged)
           catsController.catsTableView.addSubview(catsController.refreshControl) }
    }
    
}
