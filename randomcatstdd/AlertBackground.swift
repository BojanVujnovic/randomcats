//
//  AlertBaground.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/9/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit

struct AlertBackground {
    
    static let catsAlertImage = "catImage.jpg"
    static let tintColor = UIColor(red:0.310, green:0.329, blue:0.451, alpha:1.00)
    static let cornerRadius: CGFloat = 14.0
    
    let alertTitle = "Error"
    let message = "There is no Internet Connection."
    let actionTitle = "Try Again!"
    let cancelTitle = "Cancel"
    
    let serverAlertTitle = "Error"
    var serverMessage: (Int) -> String = { (error: Int) -> String in
        return "There has been a Server Error code: \(error)"
    }
    
    let serverActionTitle = "Try Again!"
    let serverCancelTitle = "Cancel"
    
    }
