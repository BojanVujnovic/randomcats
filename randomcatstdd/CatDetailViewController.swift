//
//  CatDetailViewController.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/24/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit

class CatDetailViewController: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak var catImageView: UIImageView!
   
    let navigation = Navigation()
    var cat: CatVM!
    var detailAnimation: DetailAnimationProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.catViewConfiguration()
        self.detailAnimation = DetailAnimationView(frame: DetailAnimationView.frame)
        self.addDetailAnimationView(detailAnimation)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.detailAnimation.moveImageLeft(self.detailAnimation as! DetailAnimationView, controller: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let detailAnimation = self.detailAnimation as? UIView { detailAnimation.removeFromSuperview() }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Functions
    
    //Configures Cat view
    func catViewConfiguration() {
        navigationItem.title = navigation.detailTitle
        self.catImageView.image = cat.image
    }
    
    func addDetailAnimationView(_ detailAnimation: DetailAnimationProtocol) {
        if let detailAnimation = detailAnimation as? DetailAnimationView {
           AnimationViewBridge.setCatImage(detailView: detailAnimation ,controller: self)
           self.navigationController?.view.addSubview(detailAnimation) }        
    }
    
}
