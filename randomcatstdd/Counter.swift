//
//  Counter.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/17/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import Foundation

struct Counter {
    static let oneTime = 1
    var count = 1
}

extension Counter {
    
    mutating func addCount() {        
        self.count += 1
    }
}
