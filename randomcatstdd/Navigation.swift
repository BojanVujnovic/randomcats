//
//  Navigation.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/7/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit

struct Navigation {
    
    //Properties
    let mainTitle = "Cats"
    let detailTitle = "Cat"
    let barTextColor = UIColor(red:0.984, green:0.937, blue:0.733, alpha:1.00)
    let tintColor = UIColor(red:0.984, green:0.937, blue:0.733, alpha:1.00)
    let backgroundColor = UIColor.black
    let backButtonFont =  UIFont(name: "Avenir", size: 18)
    let barStyle = UIBarStyle.black
    let titleTextFont = UIFont(name: "Avenir Heavy", size: 25.0)
    let titleTextAttributes: [String:AnyObject]?
    
    init() {
        titleTextAttributes = [NSForegroundColorAttributeName: barTextColor, NSFontAttributeName: titleTextFont!]
    }
}

extension Navigation {
    //Functions
    
    func navigationBarSettings() {
        UINavigationBar.appearance().barStyle = self.barStyle
        UINavigationBar.appearance().backgroundColor = self.backgroundColor
        //Navigation Bar title
        UINavigationBar.appearance().titleTextAttributes = self.titleTextAttributes
        //Back Bar Item text font and color
        UINavigationBar.appearance().tintColor = self.tintColor
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: self.backButtonFont!], for: UIControlState())
    }
    
}
