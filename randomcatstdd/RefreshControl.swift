//
//  RefreshControl.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/17/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit

struct RefreshControl {
    
    let frame = UIRefreshControl(frame: CGRect(x: UIScreen.main.bounds.width/2, y: 20, width: 65, height: 65))
    let backgroundColor = UIColor.black
    let tintColor = UIColor.lightYellow
}
