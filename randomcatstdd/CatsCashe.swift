//
//  ImageCashe.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 2/8/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import UIKit

class CatsCache: NSCache<CatVM, CatVM> {
   
    static let sharedCache = CatsCache()
    
    private override init() {
        super.init()
        self.name = "MyCatCache"        
        self.countLimit = 10 // Max 10 images in memory.
        self.totalCostLimit = 10*1024*1024 // Max 20MB used.
    }    
    
    func readFromCache(cat: CatVM) -> CatVM? {
        if let catFromCache = CatsCache.sharedCache.object(forKey: cat) {
            return catFromCache
        } else { return nil }
    }
    
    func writeToCache(cat: CatVM?) {
        if let catFromServer = cat {
            CatsCache.sharedCache.setObject(catFromServer, forKey: catFromServer) }
    }
    
    
}




