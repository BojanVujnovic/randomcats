//
//  SelectorExtension.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 3/2/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import UIKit

extension Selector {
    static let randomCats = #selector(CatsViewController.randomCats(_:))
    static let getLatestCats = #selector(CatsViewController.getLatestCats(_:))
    
}
