//
//  AnimationView.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/15/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit


//Random button
class RandomButton: UIButton, Shakeable {
    let delay = 0.0
    let springWithDamping: CGFloat = 0.01
    let initialVelocity: CGFloat = 0.5
}

//MARK: MainAnimationView class
class MainAnimationView: UIView, AnimationProtocol {

    //Properties
    static var frame = CGRect(x: 0, y: 13, width: 38, height: 38) //CGRect(x: 0, y: 20, width: 38, height: 38)
    var randomButton = RandomButton()
    static let randomButtonRotation = CGFloat(Double.pi)/5
    var catImageView: UIImageView?
    var catImage = "cat.png"
    var catImageOffset = CGFloat(40)
    var animationTime = AnimationTime()
    var transformScale: (sx:CGFloat,sy:CGFloat) = (sx:1.2,sy:1.2)
    var decreaseCoef: Double = 0.0
    var increaseCoef: Double = 0.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        randomButton.frame = self.bounds        
        self.addSubview(randomButton)
        self.center.x -= 10
                
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")        
    }

    
}










