//
//  CatsParserProtocol.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 3/2/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import UIKit

//MARK: Protocol for reloading data from Server in a TableView
protocol CatsLoadedDelegate: class {
    func reloadTableView()
}

//MARK: Protocol for Parsing XML file
protocol CatsParserProtocol: class {
    var data: [CatVM] { get set }
    var catsXMLParser: CatsXMLParser { get set }    
    var parserEndedDocument: Bool { get set }
    weak var loadingDelegate: CatsLoadedDelegate? { get set }    

}

extension CatsParserProtocol {
    var parserEndedDocument: Bool {
        get {
            return false   }
        set {
            parserEndedDocument = newValue
        }
    }
    
}
//MARK: Protocol for start parsing
protocol BeginParsingProtocol: class {   
   
    func beginParsing(url urlAddress:String, internet: Bool )
    func dataTaskWithRequest(urlRequest: URLRequest, completed: @escaping DownloadComplete)
    func startParse(xmlData data: Data)
}


protocol CatsManagerSettable {
    var catsManager: CatsManager? { get set }
}

extension CatsManagerSettable {
    var catsDataSourceDidSelectCatNotification: String { return "CatsDataSourceDidSelectCatNotification" }
    
}
