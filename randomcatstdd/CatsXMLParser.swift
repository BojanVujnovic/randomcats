//
//  CatsXMLParser.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/25/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import Foundation



struct CatsXMLParser {
    
    
    
      //Properties
      var xmlParser = XMLParser()
      var entryImage = String()
      var entryUrl = String()
      var weAreInsideAnItem = false
      var currentParsedElement = String()
      var xmlElement = "url"
      let xmlEndElement = "image"
      var urlString: String {
        get {            
            return CatAPI.catsURL
        } set {
        }
      }
      var dataSuccess = DataSuccess.DataFailure
    
    enum Parser: String {
        case started = "Parser has started the document successfully."
        case ended = "Parser has ended the document successfully."
    }
}

extension CatsXMLParser {
    
    mutating func addInputURL(urlAddress url: String) {
        urlString = url
    }
}
