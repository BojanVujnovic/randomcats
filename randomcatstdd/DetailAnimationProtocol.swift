//
//  DetailAnimationProtocol.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 3/2/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import UIKit

//MARK: Protocol for DetailAnimation on CatDetailViewController
protocol DetailAnimationProtocol {
    
    static var frame: CGRect { get set }
    var catView: UIImageView! { get set }
    var width: CGFloat { get set }
    var height: CGFloat { get set }
}

extension DetailAnimationProtocol {
    
    //Shows Cat image on the NavigationBar
    func showCat(_ catImageView: UIImageView?, frame: CGRect) {
        if let imageView = catImageView {
            if let view = self as? DetailAnimationView {
                view.catView = imageView
                catView.center = view.center
                catView.frame = view.bounds
                catView.contentMode = .scaleAspectFit
                catView.clipsToBounds = true
                catView.autoresizesSubviews = true
                view.addSubview(imageView)
            }
        }
    }
    
    //Moves an image to the left
    func moveImageLeft(_ detailView: DetailAnimationProtocol, controller: UIViewController) {
        if let detailView = detailView as? DetailAnimationView {
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [.curveEaseOut], animations: {
                detailView.center.x -= detailView.width * 2
            }, completion: nil) }
    }
    
    //Resizing an Image
    func imageResize (_ image:UIImage, sizeChange:CGSize) -> UIImage {
        
        let hasAlpha = false
        let scale: CGFloat = 0.0
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
}
