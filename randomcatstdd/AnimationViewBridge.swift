//
//  AnimationViewBridge.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/15/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit

struct AnimationViewBridge {
    
    //MARK: Functions
    
    //Animation View on CatsViewController scene
    static func addAnimationView(controller: UIViewController,animation: AnimationProtocol){
                AnimationViewBridge.createMainAnimation(controller, animation: animation)
    }
    
    //Creates a MainAnimationView with a Random button
    static func createMainAnimation(_ controller: UIViewController,animation: AnimationProtocol) {
               if let catsController = controller as? CatsViewController {
                  if var _animation = animation as? MainAnimationView {
                     _animation = MainAnimationView(frame: MainAnimationView.frame)
                     _animation.changeImageOfImageView(image: _animation.catImage)
                     catsController.animationContainerView = _animation
                     catsController.animationContainerView.randomButton.addTarget(catsController, action: Selector.randomCats, for: UIControlEvents.touchUpInside)                    
                     AnimationViewBridge.showMainAnimation(catsController, animation: _animation)
                  }
               }
    }
    
    //Shows a MainAnimationView on the Navigation Bar and animates the Navigation title
    static func showMainAnimation(_ controller: UIViewController,animation: AnimationProtocol) {
                 let catsController = controller as! CatsViewController
                 catsController.animationContainerView.animateTranslation(catsController)
                 catsController.animationContainerView.animateNavigationTitle(catsController)
                 catsController.navigationController?.view.addSubview(catsController.animationContainerView as! MainAnimationView)
    }
    
    //Detail AnimationView on CatDetailViewController scene
    static func setCatImage(detailView detailAnimation:DetailAnimationProtocol, controller: UIViewController) {
        
               if let detailAnimation = detailAnimation as? DetailAnimationView,let controller = controller as? CatDetailViewController {
                  let cat = controller.cat
                     if var catImage = cat?.image {
                        let catSize = CGSize(width: detailAnimation.width, height: detailAnimation.height)
                        catImage = detailAnimation.imageResize(catImage, sizeChange: catSize)
                        detailAnimation.showCat(UIImageView(image: catImage ), frame: DetailAnimationView.frame) }
                  }
               
     }
}





