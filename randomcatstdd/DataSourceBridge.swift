//
//  DataSourceBridge.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/13/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit
import Foundation
import Dispatch



//Bridge between XMLParser and UIViewController
struct DataSourceBridge {
    
    //MARK: Properties
    static let backgroundQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
    static let mainQueue = DispatchQueue.main
    
    //MARK: Functions
    
    //Set TableView data source
    static func catsTableDataSourceSettings(manager catsManager: CatsManager ,data: CatsDataSource ,tableView: UITableView) {      
        
        DataSourceBridge.backgroundQueue.async {
            DataSourceBridge.doParse(catsManager.apiManager)
        }
        DataSourceBridge.setDataSource(manager: catsManager, data: data, tableView: tableView)
    }
    
    //Set Data source for a TableView
    private static func setDataSource(manager catsManager: CatsManager ,data: CatsDataSource ,tableView: UITableView ) {
        let dataSource = data
        dataSource.catsManager = catsManager
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
    }
    
    //Parse a XML file
    private static func doParse(_ api: RESTApiManager?) {
        if let apiManager = api, let catsParser = apiManager.parser as? CatsParser {
            //Start Parsing
            apiManager.beginParsing(url: catsParser.catsXMLParser.urlString, internet: Reachability.isConnectedToNetwork() == true)
            //Calls XMLParserDelegate methods
            let catsXmlParser = catsParser.catsXMLParser
            catsParser.parser(catsXmlParser.xmlParser, didStartElement: catsXmlParser.xmlElement, namespaceURI: nil, qualifiedName: nil, attributes: [ : ])
            catsParser.parser(catsXmlParser.xmlParser, foundCharacters: catsXmlParser.currentParsedElement)
            catsParser.parser(catsXmlParser.xmlParser, didEndElement: catsXmlParser.xmlEndElement, namespaceURI: nil, qualifiedName: nil)
            catsParser.parserDidEndDocument(catsXmlParser.xmlParser)
        }
    }
    
    
    
    
}


