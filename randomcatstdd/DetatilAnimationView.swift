//
//  DeatilAnimationView.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 8/3/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit


//MARK: DetailAnimationView class
class DetailAnimationView: UIView, DetailAnimationProtocol {
    
    //Properties
    static var frame = CGRect(x: UIScreen.main.bounds.width + 42, y: 0, width: 50, height: 44)
    var catView: UIImageView!
    var detailCenter: CGFloat = 40.0
    var cornerRadius: CGFloat = 10.0
    var width: CGFloat
    var height: CGFloat
    
    override init(frame: CGRect) {
        self.width = frame.size.width
        self.height = frame.size.height
        
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        self.center.y = detailCenter        
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFit
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")        
    }
    
    
    
}

