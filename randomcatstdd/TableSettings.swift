//
//  TableSettings.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 7/6/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit

struct TableSettings {    
    private init() {}
    
    static let backgroundColor = UIColor.black
    static let contentInset = UIEdgeInsetsMake(2, 0, 0, 0)
    static let cellIdentifier = "CatCell"
    static let deleteTitle = "Remove Cat"
}
