//
//  Shakeable.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 3/3/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import UIKit

protocol Shakeable {}

extension Shakeable where Self: UIView {
    
    func shake()  {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.1
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 3.0, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 3.0, y: self.center.y))
        layer.add(animation, forKey: "position")
    }
}
