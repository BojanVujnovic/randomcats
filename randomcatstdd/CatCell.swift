//
//  CatCell.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/23/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit

//MARK: CatCell custom class for CatsTableView
class CatCell: UITableViewCell {
    
    //Properties
    @IBOutlet weak var catImageView: UIImageView!
    
    private var catVM: CatVM!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    
    //Functions
    
    //Configures a Cell in CatsTableView
    func configureCellWithCat(withCat cat: CatVM) {
        self.catVM = cat
        catImageView.image = self.catVM.image
        self.backgroundColor = TableSettings.backgroundColor        
    }
        
}
    
    
    

 
