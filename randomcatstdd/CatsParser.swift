//
//  CatsParser.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/25/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit
import Foundation


//MARK: XML Parser
class CatsParser: NSObject, XMLParserDelegate, CatsParserProtocol {

    
    //MARK: Properties
    var data = [CatVM]()
    var catsXMLParser = CatsXMLParser()
    //var parseDelegate: BeginParsingProtocol?
    weak var loadingDelegate: CatsLoadedDelegate?
    weak var catsUpdateDelegate: CatsUpdateDelegate?
    var isStarted = false
    
    override init() {
        super.init()
        //CatsParser Delegate is in this case BeginParse class , conforming to BeginParsingProtocol
        //self.parseDelegate = BeginParse(parser: self)
    }
}

extension CatsParser {
    
    //MATK: Functions
        
    //NSXMLParserDelegate protocol
    
    //Starts parsing an Element
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        
        if elementName == self.catsXMLParser.xmlElement { catsXMLParser.weAreInsideAnItem = true }
        if self.catsXMLParser.weAreInsideAnItem {
            switch elementName {
            case self.catsXMLParser.xmlElement:
                self.catsXMLParser.entryUrl = String()
                self.catsXMLParser.currentParsedElement = self.catsXMLParser.xmlElement
            default: break
            }
        }
    }
    
    //Finds Characters in the Element
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let _ = findCharacters(characters: string)
    }
    
    func findCharacters(characters string: String) -> String {
        
        if self.catsXMLParser.weAreInsideAnItem {
            switch self.catsXMLParser.currentParsedElement {
            case self.catsXMLParser.xmlElement:
                self.catsXMLParser.entryUrl += string
            default: break
            }
        }
        return catsXMLParser.entryUrl
    }
    
    //Loads Cats array with data (UIImage)
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        self.insideXmlElement(element: elementName, entryUrl: self.catsXMLParser.entryUrl)
    }
    
    //Checks if We are inside of an element and adds Image from there.
    func insideXmlElement(element elementName: String, entryUrl: String) {
        
        if catsXMLParser.weAreInsideAnItem {
            switch elementName {
            case self.catsXMLParser.xmlElement:
                 self.catsXMLParser.currentParsedElement = ""
            default: break
            }
            if elementName == self.catsXMLParser.xmlEndElement {
                self.addImageFromXmlElement(entryUrl: entryUrl)                
                self.catsXMLParser.weAreInsideAnItem = false
            }
        }
    }
    
    //Adds Cat image parsed from the XML file
    func addImageFromXmlElement(entryUrl: String)  {        
        if let url = URL(string: entryUrl), let contents = try? Data(contentsOf: url), let catImage = UIImage(data: contents) {
               let catVM = CatVM(cat: Cat(photo: catImage))
               self.data.append(catVM)
               print("Image: \(self.data.count)")
               loadingDelegate?.reloadTableView()
               catsUpdateDelegate?.updateCats()
        }
    }
    
    //Checks if parsing is finished and reloads Table View
    func parserDidEndDocument(_ parser: XMLParser) {
        isStarted = !isStarted
        if isStarted {
            print("\(CatsXMLParser.Parser.started.rawValue)\n")
        } else {
            print("\(CatsXMLParser.Parser.ended.rawValue)\n")
        }
        
    }
    
    
    
    
}
