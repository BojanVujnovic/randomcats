//
//  BeginParse.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 7/8/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit

typealias DownloadComplete = () -> ()

//MARK: Begin Parse an XML file Class
class RESTApiManager: BeginParsingProtocol {
   
    
    //MARK: Properties
    
    var dataTask: URLSessionDataTask?
    var parser: CatsParserProtocol!
    var serverError: Int?
    
    init(parser: CatsParser) {
        self.parser = parser
    }
    
    //MARK: Functions
    
    //Begins parsing
    func beginParsing(url urlAddress:String, internet: Bool ) {
        //Checks Internet connectivity
        if internet == true {
            if let url = URL(string: urlAddress) {
                let urlRequest = URLRequest(url: url)
                if self.dataTask == nil {
                    self.dataTaskWithRequest(urlRequest: urlRequest, completed: {
                        self.parser.catsXMLParser.dataSuccess = DataSuccess.DataSuccessful
                        print(self.parser.catsXMLParser.dataSuccess.rawValue)
                    })
                } else { self.dataTask!.cancel()  }
            } else {
                print("There is no such url.")
            }
        }
    }
    
    //Makes a connection with a Web server
    func dataTaskWithRequest(urlRequest: URLRequest, completed: @escaping DownloadComplete )  {
        let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
        dataTask = defaultSession.dataTask(with: urlRequest, completionHandler: { [unowned self]  (data: Data?, response: URLResponse?, error: Error?) in
            self.checkCompletionHandler(data, response: response, error: error)
        })
        dataTask?.resume()
    }
    
    //Checks Data, Response and Error
    fileprivate func checkCompletionHandler(_ data: Data?, response: URLResponse?, error: Error? ) {        
        guard error == nil else {
            print(error!.localizedDescription)
            return
        }
        guard let _ = response else {
            print("There is no response from the Server.")
            return
        }
        
        if let httpURLResponse = response as? HTTPURLResponse {
            self.findServerError(response: httpURLResponse)
        }
        
        guard data != nil  else {
            self.parser.catsXMLParser.dataSuccess = DataSuccess.DataFailure
            print(self.parser.catsXMLParser.dataSuccess.rawValue)
            return
        }
        self.startParse(xmlData: data!)
    }
    //Checks Server Error Type
    private func findServerError(response: HTTPURLResponse)  {
        switch response.statusCode {
        case ServerErrors.Internal_Server_Error.rawValue: self.serverError = ServerErrors.Internal_Server_Error.rawValue
            print("\(ServerErrors.Internal_Server_Error)")
        case ServerErrors.Bad_Gatway.rawValue: self.serverError = ServerErrors.Bad_Gatway.rawValue
            print("\(ServerErrors.Bad_Gatway)")
        case ServerErrors.Gateway_Timeout.rawValue: self.serverError = ServerErrors.Gateway_Timeout.rawValue
            print("\(ServerErrors.Gateway_Timeout)")
        case ServerErrors.Service_Unavailable.rawValue: self.serverError = ServerErrors.Service_Unavailable.rawValue
            print("\(ServerErrors.Service_Unavailable)")
        default:
            return
        }
        print("serverError = \(String(describing: serverError))")        
    }
    
    
    func startParse(xmlData data: Data) {
        
        parser.catsXMLParser.xmlParser = XMLParser(data: data)
        parser.catsXMLParser.xmlParser.delegate = parser as! CatsParser
        parser.catsXMLParser.xmlParser.parse()
        
    }
    
    
    
    
}
