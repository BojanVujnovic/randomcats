//
//  AnimationTime.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/15/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit

struct AnimationTime {
    
    static var duration = 3.0
    static let delay = 0.5
    static let spring: CGFloat = 0.9
    static let velocity: CGFloat = 0.2
    static var delayCorrection = 0.5
}

extension AnimationTime {
    
    static func increaseTime(_ times: Double) -> Double {
        self.duration += times
        return self.duration
    }
    static func decreaseTime(_ times: Double) -> Double {
        self.duration -= times
        return self.duration
    }
    static func delayCorrection(_ delay: Double) {
        self.delayCorrection = abs(delay - 0.1)
        
        
    }
}
