//
//  Cats.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/23/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit
import GameplayKit


//Cat
struct Cat {
    
    //Properties
    var photo: UIImage?
    
    init(photo: UIImage) {
        self.photo = photo
    }   
}

//Class CatVM that represents ViewModel
class CatVM: Equatable {
    
    private var cat: Cat
    
    var image: UIImage? {
        if let catImage = cat.photo {
            return catImage
        } else { return nil }
    }
    
    init(cat: Cat) {
        self.cat = cat
    }    
}

func ==(lhs: CatVM, rhs: CatVM) -> Bool {
    if lhs.image != rhs.image { return false }
    return true
}

protocol CatsUpdateDelegate: class {
    func updateCats()
}

//Class CatsManager
class CatsManager: CatsManagerProtocol, CatsUpdateDelegate {
    
    //Properties    
    var catsArray = [CatVM]()
    var catsXMLParser = CatsXMLParser()
    var apiManager: RESTApiManager?

    init(api: RESTApiManager) {
        self.apiManager = api
    }
    
    //Functions
    subscript(index: Int) -> CatVM? {
        if !catsArray.isEmpty, catsCount > 0 {
            return catsArray[index] } else {
            return nil
        }
    }
    
    //Functions
   func addCat(_ cat: CatVM )  {
       if !catsArray.contains(cat) {
          catsArray.append(cat)
          print("catsCount = \(catsCount)")
       }
    }
    
    func updateCats() {
        if let data = apiManager?.parser.data, data.count > 0 {
            self.addCat(data.last!)
        }
    }
    
    func removeCat(index: Int)  {
        if catsCount > 0 {
            self.catsArray.remove(at: index)
        }
    }
    
   func randomizeCats() {
        catsArray = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: catsArray) as! [CatVM]
//          catsArray.sort { (cat1:CatVM, cat2:CatVM) -> Bool in
//              return arc4random() < arc4random()
//          }
    }
}
