//
//  CatsDataSource.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/23/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//
import UIKit
import Dispatch


import UIKit

//MARK: CatsDataSource class
class CatsDataSource: NSObject, CatsManagerSettable {
    
    //MARK: Properties
    
    var catsManager: CatsManager?
    var cache = CatsCache.sharedCache
    
}

extension CatsDataSource: UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Functions
    
    //TableView DataSource protocol
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return catsManager?.catsCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: TableSettings.cellIdentifier, for: indexPath) as? CatCell {
            guard let manager = self.catsManager, manager.catsCount > 0 else {  fatalError()  }
             let cat = manager[indexPath.row]            
             self.presentObjectFromCashe(cat: cat!, cell: cell, indexPath: indexPath, tableView: tableView)
            tableView.backgroundColor = TableSettings.backgroundColor
            tableView.contentInset = TableSettings.contentInset
            return cell
        } else {
            return CatCell()
        }
    }
    
    //If Cashe contains the Image, Cell will be configured with it.
    private func presentObjectFromCashe(cat: CatVM, cell: CatCell, indexPath: IndexPath, tableView: UITableView) {
        //If there is an Image in the Cashe for a key
        if let catFromCache = cache.readFromCache(cat: cat) {
            //Present an Image from the Cache
            cell.configureCellWithCat(withCat: catFromCache )            
        } else {
            // Store the image in to our cache
            cache.writeToCache(cat: cat)
            // Update the cell
            DispatchQueue.main.async { 
                if let cellToUpdate = tableView.cellForRow(at: indexPath) as? CatCell, let image = cat.image {
                    //catImageView? is needed for Unit tests(asynchronious connection tests) 
                    cellToUpdate.catImageView?.image = image
                }
            }
        }
    }
    
    //TableView delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notification = Notification(name: Notification.Name.init(rawValue: catsDataSourceDidSelectCatNotification), object: self, userInfo: nil)        
        NotificationCenter.default.post(notification)
        tableView.deselectRow(at: indexPath, animated: true)
    }    
    
    //Swipe to delete
   
    //Customize Delete Button
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.normal, title: TableSettings.deleteTitle) { (rowAction:UITableViewRowAction, index:IndexPath) in
            if let manager = self.catsManager {
                manager.removeCat(index: index.row)
                print("manager.catsCount : \(manager.catsCount)")
                tableView.reloadData()
                }
        }
        deleteAction.backgroundColor = UIColor.lightYellow
        return [deleteAction]
    }
    
}


