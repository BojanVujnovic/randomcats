//
//  AnimationProtocol.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 3/2/17.
//  Copyright © 2017 Bojan Vujnovic. All rights reserved.
//

import UIKit

//MARK: Protocol for Animation on the CatsViewController
protocol AnimationProtocol {
    
    static var frame: CGRect { get set}
    var randomButton: RandomButton { get set}
    
    var catImage: String { get set}
    var catImageOffset: CGFloat { get }
    var transformScale: (sx:CGFloat,sy:CGFloat) { get }
    var decreaseCoef: Double { get set}
    var increaseCoef: Double { get set}
    
}

extension AnimationProtocol {
    
    var catImageView: UIImageView? {
        let image = UIImageView(image: UIImage(named: "cat"))
        image.frame = MainAnimationView.frame
        return image
    }
    
    //Functions
    func changeImageOfImageView(image: String?) {
        var catImage = self.catImageView
        if let image = image { catImage = UIImageView(image: UIImage(named: image)) }
        catImage?.center = self.randomButton.center
        catImage?.frame = self.randomButton.bounds
    }
    
    //Animates Cat image with TransitionCrossDissolve transition
    func transitionOfImageToButton(_ imageView: UIImageView?) {
        
        UIView.transition(with: self.randomButton, duration: AnimationTime.duration,  options: .transitionCrossDissolve, animations: {
            if let image = imageView {  self.randomButton.addSubview(image)     }
        }, completion: nil)
    }
    //Rotates the RandomButton image
    func rotateButtonImage(_ imageView: UIImageView?) {
        UIView.animate(withDuration: AnimationTime.duration, delay: randomButton.delay, usingSpringWithDamping: randomButton.springWithDamping, initialSpringVelocity: randomButton.initialVelocity, options: UIViewAnimationOptions.curveEaseOut, animations: {
            if let _ = imageView {
                let angle = MainAnimationView.randomButtonRotation
                self.randomButton.transform = CGAffineTransform(rotationAngle: angle)
            }
        }) { (_) in
            UIView.animate(withDuration: AnimationTime.duration/8, animations: {
                self.randomButton.transform = CGAffineTransform.identity
            })
        }
    }
    //Decreases the size of the RundomButton image
    func scaleButtonImage(_ imageView: UIImageView?) {
        //Decreases Cat's size by 15%
        UIView.animate(withDuration: AnimationTime.duration, delay: AnimationTime.delay - AnimationTime.delayCorrection, usingSpringWithDamping: AnimationTime.spring, initialSpringVelocity: AnimationTime.velocity, options: [], animations: {
            if let _ = imageView {
                if let animation = self as? UIView {
                    animation.transform = CGAffineTransform(scaleX: self.transformScale.sx, y: self.transformScale.sy)
                    self.rotateButtonImage(self.catImageView)
                }
            }
            
        }, completion: nil)
    }
    //Returns the RandomButton image to original size
    func buttonImageOriginalSize(_ imageView: UIImageView?) {
        //Returns Cat's size to original value
        UIView.animate(withDuration: AnimationTime.duration, delay: AnimationTime.delay, usingSpringWithDamping: AnimationTime.spring, initialSpringVelocity: AnimationTime.velocity, options: [], animations: {
            if let _ = imageView {
                if let animation = self as? UIView {
                    animation.transform = CGAffineTransform.identity }
            }
        }, completion: nil)
    }
    
    //Translates an image to the right
    func animateTranslation(_ controller: UIViewController) {
        let coef = self.decreaseCoef
        let timeForAnimation = AnimationTime.decreaseTime(coef)
        //Moves AnimationView to right
        UIView.animate(withDuration: timeForAnimation, animations: {
            if let view = self as? UIView, let catImage = self.catImageView {
                view.center.x += controller.view.bounds.size.width - self.catImageOffset
                catImage.center = self.randomButton.center
                catImage.frame = self.randomButton.bounds
            }
        })
    }
    
    //Moves the navigation title from up to down
    func animateNavigationTitle(_ controller: UIViewController) {
        //Moves Navigation title down
        UIView.animate(withDuration: AnimationTime.increaseTime(self.increaseCoef), delay: AnimationTime.delay, options: [], animations: {
            if let catsController = controller as? CatsViewController {
                catsController.navigationItem.title = catsController.navigation.mainTitle }
            
        }, completion: nil)
    }
    
    //Radnom Cats Animation
    func animateRandomButton(catImage image: UIImageView ) {
        self.scaleButtonImage(image)
        self.rotateButtonImage(image)
        self.buttonImageOriginalSize(image)        
    }
    
    
}
