//
//  Alert.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/7/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import UIKit



//MARK: Cats Alert class
class CatsAlert: CatsAlertProtocol {
    
    //MARK: Properties
    
    var alertBackgroundImage: UIImageView
    static var controller = UIViewController()
    static let sharedInstance = CatsAlert(controller: CatsAlert.controller , imageName: AlertBackground.catsAlertImage)
    let alertBackground = AlertBackground()
    
    fileprivate init(controller: UIViewController, imageName: String) {
        self.alertBackgroundImage = UIImageView(image: UIImage(named: imageName))
    }
    

}
extension CatsAlert {
    
    //MARK: Functions
    
    //Alert
    
    //Triggers alert when no Internet connection
    func networkAlert(controller: UIViewController)   {
        
        if Reachability.isConnectedToNetwork() == false {
           let _ = self.setBackgroundImage(controller)
           let alert = self.createCatAlert(CatsAlert.controller, alertTitle: alertBackground.actionTitle, message: alertBackground.message, actionTitle: alertBackground.actionTitle, cancelTitle: alertBackground.cancelTitle)
           controller.present(alert, animated: true, completion: nil)
        }
    }
    
    func serverErrorAlert(controller: UIViewController, errorCode: (Int) -> String, code: Int)  {
        let _ = self.setBackgroundImage(controller)
        let alert = self.createCatAlert(CatsAlert.controller, alertTitle: alertBackground.serverAlertTitle, message: errorCode(code), actionTitle: alertBackground.serverActionTitle, cancelTitle: alertBackground.serverCancelTitle)
        controller.present(alert, animated: true, completion: nil)        
    }
    
    //Creates an Alert Controller
    func createCatAlert(_ controller: UIViewController,alertTitle: String, message: String, actionTitle: String, cancelTitle: String) -> UIViewController {
        let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: actionTitle, style: UIAlertActionStyle.default) { (action:UIAlertAction) -> Void in
            self.alertConnectivity(controller)        }
        let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertActionStyle.cancel) { (action:UIAlertAction) -> Void in
            self.alertConnectivity(controller)        }
        
        alert.addAction(alertAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = AlertBackground.tintColor
        alert.view.layer.cornerRadius = AlertBackground.cornerRadius
        return alert
    }
    
    
        
    //Sets backgroung image of Alert
    fileprivate func setBackgroundImage(_ controller: UIViewController) -> UIImage {
        alertBackgroundImage.frame.size = UIScreen.main.bounds.size
        alertBackgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        controller.view.addSubview(alertBackgroundImage)
        return alertBackgroundImage.image!
    }
    
    //Removes backgroung image of Alert
    fileprivate func removeBackgroundImage(_ controller: UIViewController) -> UIImage {
        alertBackgroundImage.removeFromSuperview()
        return alertBackgroundImage.image!
    }
    
    //Checks Internet connection and performs proper action
    fileprivate func alertConnectivity(_ controller: UIViewController) {
            if let catsController = controller as? CatsViewController {
                if Reachability.isConnectedToNetwork() == true {
                    catsController.activityIndicator.startAnimating()
                    let _ = self.removeBackgroundImage(controller)
                    catsController.dismiss(animated: true, completion: nil)                    
                    catsController.setDataSource(manager: catsController.catsManager)
                 } else {
                   // catsController.catsParser = nil
                    return self.networkAlert(controller: catsController)  }
            }        
    }   
    
}


//Factory Patern for choosing Alert if there are more than one of them
func getAlert(controller viewController: UIViewController) ->  CatsAlertProtocol? {
    
    if viewController is CatsViewController  {
        CatsAlert.controller = viewController
        return CatsAlert.sharedInstance
    } else {   return nil   }
}


