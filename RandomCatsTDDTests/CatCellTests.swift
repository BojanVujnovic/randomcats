//
//  CatCellTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/24/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//
import UIKit
import Foundation
import XCTest
@testable import RandomCatsTDD

class CatCellTests: XCTestCase {
    
    var tableView: UITableView!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let catsController = storyboard.instantiateViewController(withIdentifier: "CatsController") as! CatsViewController
        
        _ = catsController.view
        
        tableView = catsController.catsTableView
        tableView.dataSource = FakeDataSource()
        
    }

    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCatCell_HasCatImageView() {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CatCell") as! CatCell
        XCTAssertNotNil(cell.catImageView)
    }
    
    func testConfigureCellWithCat_SetsCatImage() {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CatCell") as! CatCell
        let catImage = UIImage(named: "catImage.jpg")
        let cat = Cat(photo: catImage!)       
        
        cell.configureCellWithCat(withCat: CatVM(cat: cat))
        XCTAssertEqual(cell.catImageView.image, catImage)
    }
    
    
    
}


extension CatCellTests {


    class FakeDataSource: NSObject, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return CatCell()
        }
    }









}
