//
//  CatsDataSourceTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/23/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//
import UIKit
import XCTest
@testable import RandomCatsTDD

extension Selector {
    static let didReceiveNotification = #selector(CatsDataSourceTests.didReceiveNotification(_:))
}

class CatsDataSourceTests: XCTestCase {
    
    var catsController: CatsViewController!
    var catsManager: CatsManager!
    var catsData: CatsDataSource!
    var tableView: UITableView!
    var cat: Cat!
    var postedNotification: Notification?
    var selectedIndexPath: IndexPath?
    
    override func setUp() {
        super.setUp()
        
        let catsParser = CatsParser()
        self.catsManager = CatsManager(api: RESTApiManager(parser: catsParser))
        self.catsData = CatsDataSource()
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        catsController = storyboard.instantiateViewController(withIdentifier: "CatsController") as! CatsViewController
        catsController.catsManager = catsManager
        catsData.catsManager = catsController.catsManager
        _ = catsController.view
        tableView = catsController.catsTableView
        tableView.dataSource = catsData
        tableView.delegate = catsData
        let catImage = UIImage(named: "catImage.jpg")
        cat = Cat(photo: catImage!)
        selectedIndexPath = IndexPath(row: 0, section: 0)
        let _ = NotificationCenter.default.addObserver(self, selector: Selector.didReceiveNotification, name: NSNotification.Name.init(rawValue: self.catsData.catsDataSourceDidSelectCatNotification), object: nil)
        
        
    }
    
    override func tearDown() {
        super.tearDown()
        postedNotification = nil
        NotificationCenter.default.removeObserver(self)
    }
    
    func didReceiveNotification(_ notification: Notification) {
        postedNotification = notification
    }
    
    func testNumberOfSections_IsOne() {
        
        let numberOfSections = tableView.numberOfSections
        XCTAssertEqual(numberOfSections, 1)
    }
    
    func testNumberRowsInFirstSection_IsCatsCount() {
        let catVM = CatVM(cat: cat)
        let _ = catsController.catsManager?.addCat(catVM)
        
        let numberOfRows = tableView.numberOfRows(inSection: 0)
        XCTAssertEqual(numberOfRows, 1)
    }
    
    func testCellForRow_ReturnsCatCell() {
        let catVM = CatVM(cat: cat)
        let _ = catsData.catsManager?.addCat(catVM)
        tableView.reloadData()
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is CatCell)
    }
    
    func testCellForRow_DequeuesCell() {
        
        let mockTableView = MockTableView.mockTableViewWithDataSource(dataSource: catsData)
        let catVM = CatVM(cat: cat)
        let _ = catsData.catsManager?.addCat(catVM)
        mockTableView.reloadData()
        
        let _ = mockTableView.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(mockTableView.cellGotDequeued)
    }
    
    func testConfigureCell_GetsCalledInCellForRow() {
        
        let mockCatsDataSource = MockCatsDataSource()
        let mockTableView = MockTableView.mockTableViewWithDataSource(dataSource: mockCatsDataSource)
        mockTableView.reloadData()
        
        let cell = mockTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! MockCatCell
        XCTAssertEqual(cell.cat, CatVM(cat: cat))
        
    }
    
    func testSelectingACell_SendsNotification() {
       if let table = self.tableView, let path = self.selectedIndexPath {
            _ = catsData.tableView(table, didSelectRowAt: path)
        
           XCTAssertEqual(postedNotification!.name.rawValue, catsData.catsDataSourceDidSelectCatNotification)
        }
    }
    
    
}


extension CatsDataSourceTests {
    
    class MockTableView: UITableView {
        
        var cellGotDequeued = false
        
        override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        }
        
        class func mockTableViewWithDataSource(dataSource: UITableViewDataSource) -> MockTableView {
            let mockTableView = MockTableView()
            mockTableView.dataSource = dataSource
            mockTableView.register(MockCatCell.self , forCellReuseIdentifier: "CatCell")
            return mockTableView
        }
    }
    
    class MockCatCell: CatCell {
        
        var cat: CatVM?
        
        
        override func configureCellWithCat(withCat cat: CatVM) {
            self.cat = cat
        }
    }
    
    class MockCatsDataSource: NSObject, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CatCell") as! MockCatCell
            let cat = Cat(photo: UIImage(named: "catImage.jpg")!)
            let catVM = CatVM(cat: cat)
            cell.configureCellWithCat(withCat: catVM)
            return cell
        }
    }
    
    
    
}





















