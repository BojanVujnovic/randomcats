//
//  AsynchroniousConnectionTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/3/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import XCTest
import Foundation
@testable import RandomCatsTDD

class AsynchroniousConnectionTests: XCTestCase {    
    
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testAsynchronousURLConnection() {
        
        let URL = Foundation.URL(string: "http://thecatapi.com/api/images/get?format=xml&results_per_page=3")!
        
        let expectation = self.expectation(description: "getURL")
        let session = URLSession.shared
        let task = session.dataTask(with: URL, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            
            XCTAssertNotNil(data, "Data should not be nil")
            XCTAssertNil(error, "Error should be nil")
            if let HTTPResponse = response as? HTTPURLResponse, let responseURL = HTTPResponse.url, let MIMEType = HTTPResponse.mimeType {
                XCTAssertEqual(responseURL.absoluteString, URL.absoluteString, "HTTP response URL should be equal to original URL")
                XCTAssertEqual(HTTPResponse.statusCode, 200, "HTTP response status code should be 200")
                XCTAssertTrue(MIMEType == "text/html" || MIMEType == "text/xml", "HTTP response content type should be text/html or text/xml")
            } else {
                XCTFail("Response was not NSHTTPURLResponse")
            }
            expectation.fulfill()
        })
        task.resume()
        
        waitForExpectations(timeout: 5.0 ) { (error: Error?) in
            if let error = error {
                XCTAssertNil(error, error.localizedDescription)            }
            
            task.cancel()
        }
        
    }
   
}
