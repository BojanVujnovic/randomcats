//
//  InternetTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/9/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import XCTest
@testable import RandomCatsTDD

class InternetTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInternet_ShouldReturnTrueIfHas() {
        
        let internet = true
        let testedInternet = Reachability.isConnectedToNetwork()
        XCTAssertEqual(testedInternet, internet)
    }
    
}
