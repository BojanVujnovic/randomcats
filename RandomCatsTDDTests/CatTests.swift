//
//  CatsTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/23/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import XCTest
@testable import RandomCatsTDD

class CatTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit_ShouldTakeImage() {
        let photo = Cat(photo: UIImage())
        XCTAssertNotNil(photo, "Photo should not be nil")
    }
    
}
