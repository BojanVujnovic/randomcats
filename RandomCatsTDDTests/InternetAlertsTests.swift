//
//  InternetAlertsTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/9/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import XCTest
@testable import RandomCatsTDD

class InternetAlertsTests: XCTestCase {
    
    var catsController: CatsViewController!
    var alertController: CatsAlert!
    
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        catsController = storyboard.instantiateViewController(withIdentifier: "CatsController") as! CatsViewController
        catsController.catsManager = CatsManager(api: RESTApiManager(parser: CatsParser()))
        
        _ = catsController.view
        alertController = getAlert(controller: self.catsController) as! CatsAlert        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testSetBackgroundImage_ShouldSetCatImage() {
        
        let inputImage = UIImage(named: AlertBackground.catsAlertImage)
        if Reachability.isConnectedToNetwork() == false {
        let setImage = alertController.setBackgroundImage(catsController)
            XCTAssertEqual(setImage, inputImage) }
    }
    
    func testRemoveBackgroundImage_ShoudRemoveCatImage() {
        let inputImage = UIImage(named: "catImage.jpg")
        if Reachability.isConnectedToNetwork() == false {
            let removedImage = alertController.removeBackgroundImage(catsController)
            XCTAssertEqual(removedImage, inputImage) }
    }
    
    func testCreateAlert_ShouldCreateAlert() {
        let alertBackground = AlertBackground()
        
        let alert = alertController.createCatAlert(self.catsController, alertTitle: alertBackground.alertTitle, message: alertBackground.message, actionTitle: alertBackground.actionTitle, cancelTitle: alertBackground.cancelTitle)
        XCTAssertTrue(alert is UIAlertController)
    }
    
    func testAlertConnectivity_ShoulCallParsing() {
        
        if Reachability.isConnectedToNetwork() == true {
            alertController.alertConnectivity(catsController)
            XCTAssertNotNil(catsController.catsManager?.catsArray)
            XCTAssertNotNil(alertController.removeBackgroundImage(catsController))
            XCTAssertTrue(catsController.activityIndicator.hidesWhenStopped == true, "Activity Indicator has not stopped") }
    }
    

    func testAlertConnectivity_WhenNoConnection() {
        if Reachability.isConnectedToNetwork() == false  {
            alertController.alertConnectivity(catsController)
            XCTAssertNil(catsController.catsData, "CatsParser should be nil if there is no internet") }
    }

    
    func testNetworkAlert_ShoulPopUpAlertMessage() {
        
        _ = alertController.networkAlert(controller: catsController)
        let alert = UIAlertController(title: "Test", message: "Test", preferredStyle: UIAlertControllerStyle.alert)
        XCTAssertNotNil(catsController.present(alert, animated: true, completion: nil), "Alert has been presented.")
    }
    
    
    func testGetAlert_ShouldChooseProperAlert() {
        
        let inputController = catsController
        let outputAlert = getAlert(controller: inputController!)
        
        XCTAssertTrue(outputAlert is CatsAlert,"Alert should be of CatsAlert type")
    }
    
    
    
}









