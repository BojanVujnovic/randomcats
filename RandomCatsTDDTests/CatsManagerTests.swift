//
//  CatsManagerTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/23/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import XCTest
@testable import RandomCatsTDD

class CatsManagerTests: XCTestCase {
    
    var catsManager: CatsManager!
    
    override func setUp() {
        super.setUp()
        let catsParser = CatsParser()
        let apiManager = RESTApiManager(parser: catsParser)
        catsManager = CatsManager(api: apiManager)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCatsCount_Initially_ShouldBeZero() {
        XCTAssertEqual(catsManager.catsArray.count, 0, "Initially cats count should be 0")
    }
    
    func testAddCat_AfterAddingOneCat_IsOne() {
        let cat = Cat(photo: UIImage())
        catsManager.addCat(CatVM(cat: cat))
        XCTAssertEqual(catsManager.catsArray.count, 1, "catsCount should be 1")
    }
    
    func test_AddingTheSameCat_DoesNotIncreaseCount() {
        
        let catImage = UIImage(named: "catImage")
        if let image = catImage {
            let cat = Cat(photo: image)
           catsManager.addCat(CatVM(cat: cat))
           catsManager.addCat(CatVM(cat: cat))
           XCTAssertEqual(catsManager.catsArray.count, 1) }
    }
    
    func testRandomCats_ShouldRandomizeCats() {
        
        XCTAssertNotNil(catsManager.randomizeCats(), "Cats array should be random")
        
    }
    
    

}
