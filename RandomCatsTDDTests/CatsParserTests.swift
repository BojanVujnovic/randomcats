//
//  CatsParserTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/25/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//
import UIKit
import XCTest
@testable import RandomCatsTDD

class CatsParserTests: XCTestCase {
    
    var catsParser: CatsParser!
    var mockCatsParser: MockCatsParser!
    var tableView = UITableView()
    var activity = UIActivityIndicatorView()
    
    let inputURL = "http://thecatapi.com/api/images/get?format=xml&results_per_page=1"
    
    override func setUp() {
        super.setUp()
        mockCatsParser = MockCatsParser()
       catsParser = CatsParser()
        
        
    }
    
    override func tearDown() {
        
        
        super.tearDown()
    }
    
    func testConvenienceInit_ShouldPassTableAndParserMethods() {
        
      //  XCTAssertEqual(catsParser.tableView, tableView)
        
    }
    
    func testInit_ShouldCallParserMethods() {
        
        let catParser = CatsParser()
        let outputCats = catParser.data
        XCTAssertNotNil(outputCats)
    }
    
    
    func testBeginParsing_ShouldPassURLAddress() {
        
        mockCatsParser.beginParsing(url: catsParser.catsXMLParser.urlString, internet: true)
        
        XCTAssertTrue(mockCatsParser.url != nil , "URL should have been passed")
        XCTAssertTrue(mockCatsParser.isParsed == true, "Data task should be nil")
        
    }
    
    func testStartParse_ShouldStartParsing() {
        
           let url = URL(string: inputURL)
           if let data = try? Data(contentsOf: url!) {
              mockCatsParser.startParse(xmlData: data ) }
        
           XCTAssertTrue(mockCatsParser.catsManager.catsXMLParser.xmlParser.publicID != "", "")
           XCTAssertTrue(mockCatsParser.catsManager.catsXMLParser.xmlParser.delegate is MockCatsParser!, "CatsManager should be a delegate of xmlParser")
    }
    
    
    func testParserDidStart_ShouldReturnCurrentParsedElement() {
        let inputElement = catsParser.catsXMLParser.xmlElement
        catsParser.parser(catsParser.catsXMLParser.xmlParser, didStartElement: inputElement, namespaceURI: nil, qualifiedName: nil, attributes: [:])
        let currentParsedElement = catsParser.catsXMLParser.currentParsedElement
     
        XCTAssertEqual(currentParsedElement, inputElement)
     }
    
    func testParserFoundCharacters_ShouldFindCharacters() {
        let inputParsedElement = "url"
        
        mockCatsParser.catsManager.catsXMLParser.currentParsedElement = inputParsedElement
        mockCatsParser.catsManager.catsXMLParser.xmlElement = inputParsedElement
        mockCatsParser.catsManager.catsXMLParser.weAreInsideAnItem = true
        mockCatsParser.parser(mockCatsParser.catsManager.catsXMLParser.xmlParser, foundCharacters: inputParsedElement)
        XCTAssertTrue(mockCatsParser.catsManager.catsXMLParser.entryUrl != "", "Parser should have found characters")
    
    }
    
    func testParserDidEndElement_ShouldAddImageToCatsArray() {
        let inputParser = catsParser.catsXMLParser.xmlParser
        let parsedUrl = "http://24.media.tumblr.com/tumblr_lwy0vxSqQd1qif29do1_250.gif"
        catsParser.catsXMLParser.entryUrl = parsedUrl
        catsParser.catsXMLParser.weAreInsideAnItem = true
        _ = catsParser.parser(inputParser, didEndElement: parsedUrl, namespaceURI: nil, qualifiedName: nil)
        XCTAssertNotNil(catsParser.data, "An Image should have been added to CatsArray")
    }
    
    func testParserDidEndDocument_ShouldEndDocument() {
        let inputParser = catsParser.catsXMLParser.xmlParser
        catsParser.catsXMLParser.urlString = inputURL
        _ = catsParser.parserDidEndDocument(inputParser)
        
        XCTAssertTrue(catsParser.isStarted == true , "Parser should have ended the document successfully.")
    }
    
    
}


extension CatsParserTests {
    
    
    class MockCatsParser: NSObject, XMLParserDelegate {
        
        var catsManager = CatsManager(api: RESTApiManager(parser: CatsParser()))
        var dataTask: URLSessionDataTask?
        var isParsed: Bool!
        var url: URL?
        var parsed = false
        
        //Begins parsing
        func beginParsing(url urlAddress:String, internet: Bool ) {
            //Checks Internet connectivity
            if internet == true {
                if let url = URL(string: urlAddress) {
                    self.url = url
                    let urlRequest = URLRequest(url: url)
                    if self.dataTask == nil {
                        
                        self.dataTaskWithRequest(urlRequest: urlRequest)
                        isParsed = true
                    } else { self.dataTask!.cancel()  }
                } else {  print("There is no such url.")   }
            }
        }
        
        //Makes a connection with a Web server
        func dataTaskWithRequest(urlRequest: URLRequest)  {
            
            let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
            dataTask = defaultSession.dataTask(with: urlRequest, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
                self.checkCompletionHandler(data, response: response, error: error)
                
                self.catsManager.catsXMLParser.dataSuccess = DataSuccess.DataSuccessful
                print(self.catsManager.catsXMLParser.dataSuccess.rawValue)        })
            
            dataTask?.resume()
        }
        
        //Checks Data, Response and Error
        fileprivate func checkCompletionHandler(_ data: Data?, response: URLResponse?, error: Error? ) {
            
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            guard let _ = response else {
                if let errorResponse = response as? HTTPURLResponse {
                    print("HTTPURL Response error , status code: \(errorResponse.statusCode)") }
                return
            }
            guard data != nil  else {
                self.catsManager.catsXMLParser.dataSuccess = DataSuccess.DataFailure
                print(self.catsManager.catsXMLParser.dataSuccess.rawValue)
                return
            }
            self.startParse(xmlData: data!)
        }
        
        func startParse(xmlData data: Data) {
            catsManager.catsXMLParser.xmlParser = XMLParser(data: data)
            catsManager.catsXMLParser.xmlParser.delegate = self
            catsManager.catsXMLParser.xmlParser.parse()
            
        }

        
        func parser(_ parser: XMLParser, foundCharacters string: String) {
            
            let _ = findCharacters(characters: string)
        }
        
        func findCharacters(characters string: String) -> String {
            
            if catsManager.catsXMLParser.weAreInsideAnItem {
                
                switch self.catsManager.catsXMLParser.currentParsedElement {
                case self.catsManager.catsXMLParser.xmlElement:
                    self.catsManager.catsXMLParser.entryUrl += string
                default: break
                }
            }
            return catsManager.catsXMLParser.entryUrl
        }
        
        
        
        
    }
    
    
    
    
    
    
    
    
}

