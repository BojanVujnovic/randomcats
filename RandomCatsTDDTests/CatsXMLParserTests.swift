//
//  CatsXMLParserTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/25/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import XCTest
@testable import RandomCatsTDD

class CatsXMLParserTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit_ShouldInitiazeCatsXMLParser() {
        let catsXMLParser = CatsXMLParser()
        XCTAssertNotNil(catsXMLParser, "CatsXMLParser should not be nil")
    }
    
    func testAddInputURL_ShouldAddURLToCatsXMLParser() {
        let inputURL = "http://thecatapi.com/api/images/get?format=xml&results_per_page=100"
        var catsXMLParser = CatsXMLParser()
        _ = catsXMLParser.addInputURL(urlAddress: inputURL)
        
    }
   
}
