//
//  DetailAnimationViewTests.swift
//  randomcatstdd
//
//  Created by Bojan Vujnovic on 8/4/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import XCTest
@testable import RandomCatsTDD

class DetailAnimationViewTests: XCTestCase {
    
    var detailController: CatDetailViewController!    
    var catsManager: CatsManager!
    var tableView = UITableView()
    var activity = UIActivityIndicatorView()
    var navigation: UINavigationController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.detailController = storyboard.instantiateViewController(withIdentifier: "CatDetailController") as! CatDetailViewController
        _ = detailController.view
        
        navigation = storyboard.instantiateInitialViewController() as! UINavigationController
        
        let catsParser = CatsParser()
        self.catsManager = CatsManager(api: RESTApiManager(parser: catsParser))
        let cat = Cat(photo: UIImage(named: "cat.png")!)
        catsManager.addCat(CatVM(cat: cat))
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAnimationOfView_ShouldShowCatView() {
        
        let animationView = DetailAnimationView(frame: DetailAnimationView.frame)
        let cat = Cat(photo: UIImage(named: "cat.png")!)
        detailController.cat = CatVM(cat: cat)
        AnimationViewBridge.setCatImage(detailView: animationView , controller: detailController)
        self.navigation.view.addSubview(animationView)
        
        let inputPosition = CGPoint(x: UIScreen.main.bounds.width + 67,y: CGFloat(40))
        XCTAssertTrue(animationView.superview == navigation.view, "NavigationController should be superview of DeatailAnimationView")
        XCTAssertEqual(animationView.center.x, inputPosition.x)
        XCTAssertEqual(animationView.center.y, inputPosition.y)
        
    }
    
    
}
