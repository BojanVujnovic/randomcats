//
//  CatDetailViewControllerTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/24/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//
import UIKit
import XCTest
@testable import RandomCatsTDD

class CatDetailViewControllerTests: XCTestCase {
    
    var catDetail: MockCatDetailController!
    
    override func setUp() {
        super.setUp()
        
        
        catDetail = MockCatDetailController()
        
        _ = catDetail.view
    }
    
    override func tearDown() {
        
                
        super.tearDown()
    }
    
    func test_HasCatImageView() {
        

        catDetail.catViewConfiguration()
        XCTAssertNotNil(catDetail.catImageView)
    }
    
    func testSettingCatInfo_SetsImageToCatImageView() {
        
        XCTAssertEqual(catDetail.catImageView.image, catDetail.catInfo.cats!.catsArray[0].image)
    }
}


extension CatDetailViewControllerTests {
    
    class MockCatDetailController: UIViewController {
        
        //Properties
        var catImageView = UIImageView()
        
        var catInfo: (cats:CatsManager?,catIndex: Int) = (CatsManager(api: RESTApiManager(parser: CatsParser())), 0)
        
        override func viewDidLoad() {
            catViewConfiguration()
        }
        
        //Configures Cat view
        func catViewConfiguration() {
            let catsManager = CatsManager(api: RESTApiManager(parser: CatsParser()))
            let catImage = UIImage(named: "catImage.jpg")
            let cat = Cat(photo: catImage!)
            catsManager.addCat(CatVM(cat: cat))
            self.catInfo = (catsManager, 0)
            catImageView.image = catInfo.cats!.catsArray[0].image
            
        }
    }
    
    
    
    
    
}
