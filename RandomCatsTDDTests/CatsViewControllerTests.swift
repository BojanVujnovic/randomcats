//
//  CatsViewControllerTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 5/23/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//
import UIKit
import XCTest
@testable import RandomCatsTDD

class CatsViewControllerTests: XCTestCase {
    
    var catsController: CatsViewController!
    var activity = UIActivityIndicatorView()
    
    var repetitions = 1...10
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        catsController = storyboard.instantiateViewController(withIdentifier: "CatsController") as! CatsViewController
        _ = catsController.view
        let catsParser = CatsParser()
        self.catsController.catsManager = CatsManager(api: RESTApiManager(parser: catsParser))
        AnimationViewBridge.addAnimationView(controller: catsController, animation: MainAnimationView())
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
   
    func test_TableViewIsNotNilAfterViewDidLoad() {
         XCTAssertNotNil(catsController.catsTableView)
    }
    
    func testViewDidLoad_ShouldSetTableViewDataSource() {
        self.catsController.setDataSource(manager: self.catsController.catsManager)
        XCTAssertNotNil(catsController.catsTableView.dataSource)
        XCTAssertTrue(catsController.catsTableView.dataSource is CatsDataSource)
    }
    
    func testViewDidLoad_ShouldSetTableViewDelegate() {
        self.catsController.setDataSource(manager: self.catsController.catsManager)
        XCTAssertNotNil(catsController.catsTableView.delegate)
        XCTAssertTrue(catsController.catsTableView.delegate is CatsDataSource)
    }
    
    func testViewDidLoad_ShouldSetDelegateAndDataSourceToTheSameObject() {
        let dataSource = catsController.catsTableView.dataSource as? CatsDataSource
        let delegate = catsController.catsTableView.delegate as? CatsDataSource
        XCTAssertEqual(dataSource, delegate)
    }

    func testAnimateOnce_ShouldAnimateImageOnce() {
        for _ in repetitions {
           let animated = catsController.animateOnce(catsController.counter)
            XCTAssertTrue(animated, "Should an Image has been animated once.")
        }
    }
    
    func testUnhideAnimation_ShouldShowAnimation() {
        let animationHidden = catsController.unhideAnimation(catsController.animationContainerView)
        XCTAssertFalse(animationHidden, "Should show a Animation")
    }
    
    func testRefreshControlTableView_ShouldAddRefreshControll() {
        self.catsController.catsManager = nil
        RefreshControlBridge.refreshControlTableView(controller: catsController)
        
        //XCTAssertTrue(catsController.catsManager != nil , "Refresh control should have called getLatestCats")
    }
    
    func testAddAnimationView_ShouldAddSubviewToCatsViewControllerView() {
        
        if let animationView = catsController.animationContainerView as? MainAnimationView {
            XCTAssertTrue(animationView.superview == catsController.navigationController?.view, "AnimationContainerView should be subview of CatsViewController NavigationController's view") }
    }
    
    func testGetLatestCats_ShouldParseLatestCats() {
        self.catsController.catsData = nil
        let refresh = UIRefreshControl()
        let cats = self.catsController.catsManager!
        if refresh.isRefreshing == true && cats.catsCount == CatAPI.catsNumber  {
            catsController.getLatestCats(refresh)
            XCTAssertTrue(catsController.catsData != nil , "Method getLatestCats should create CatsDataSource")
        }
    }
    
    func testRandomCats_CallsRandomizeCats() {
        
        let mockCatsManager = MockCatsManager()
        let inputCatsArray = mockCatsManager.catsArray
        
        for _ in 1...mockCatsManager.randomLevel {
            mockCatsManager.randomizeCats()
            let outputCatsArray = mockCatsManager.catsArray
            XCTAssertNotEqual(outputCatsArray, inputCatsArray)
        }
    }
    
    func testViewDidLoad_SetsCatsManagerToCatsData() {
        self.catsController.setDataSource(manager: self.catsController.catsManager)
        XCTAssertTrue(catsController.catsManager === catsController.catsData.catsManager)
    }
    
    
    func testPrepareForSegue_ShouldTransitionToDestinationVC() {
        let inputIdentifier = "toCatDetail"
        let mockController = MockViewController()
        let _ = mockController.performSegue(withIdentifier: inputIdentifier, sender: nil)
        XCTAssertEqual(mockController.segueIdentifier, inputIdentifier )
    }
    
    func testThatArgumentToPassIsPassedOnSegue() {
        //Arrange
        let mockController = MockViewController()
        let cat = Cat(photo: UIImage(named: "catImage")!)
        let catVM =  CatVM(cat: cat)
        mockController.argumentToPass = catVM
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let catDetail = storyboard.instantiateViewController(withIdentifier: "CatDetailController") as! CatDetailViewController
        let segue = UIStoryboardSegue(identifier: "toCatDetail", source: mockController, destination: catDetail)
        let _ = mockController.prepare(for: segue, sender: nil)
        
        //Assert
        XCTAssertEqual(catDetail.cat, mockController.argumentToPass)
    }

}

extension CatsViewControllerTests {
    
    class MockViewController: CatsViewController {
        var segueIdentifier: String?
        var argumentToPass: CatVM?
        
        override func performSegue(withIdentifier identifier: String?, sender: Any?) {
            segueIdentifier = identifier
        }
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let catIdentifier = "toCatDetail"
            if segue.identifier == catIdentifier {
                let catDetailController = segue.destination as! CatDetailViewController
                catDetailController.cat = argumentToPass!
                
            } else { return print("There is no \"\(catIdentifier)\" Segue identifier") }
        }
    }
    
    class MockCatsManager {
        
        var catsArray = [1,2,3,4,5,6,7,8,9,10]
        var randomLevel = 1_000
        var catsCount: Int { return catsArray.count }
        
        func randomizeCats() {
            catsArray.sort { (cat1:Int, cat2:Int) -> Bool in
                return arc4random() < arc4random()
            }
        }
    }
    
    class MockCatsData: CatsDataSource {
        
        var catsController: CatsViewController!
        
        init(controller: UIViewController) {            
            catsController = controller as! CatsViewController
            super.init()
        }
        
        override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            catsController.present(CatDetailViewController(), animated: true, completion: nil)
        }
        
    }
    
    class MockNavigationController : UINavigationController {
        
          var pushedViewController: UIViewController?
        
          override func pushViewController(_ viewController: UIViewController, animated: Bool) {
                pushedViewController = viewController
                super.pushViewController(viewController, animated: animated)
          }        
        
    }
    
    
    
}




