//
//  AnimationViewTests.swift
//  RandomCatsTDD
//
//  Created by Bojan Vujnovic on 6/15/16.
//  Copyright © 2016 Bojan Vujnovic. All rights reserved.
//

import XCTest
@testable import RandomCatsTDD

class AnimationViewTests: XCTestCase {
    
    var catsController: CatsViewController!
    
    
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        catsController = storyboard.instantiateViewController(withIdentifier: "CatsController") as! CatsViewController
        _ = catsController.view     
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAnimationOfView_ShouldAnimateView() {
        let animationView = MainAnimationView()
        AnimationViewBridge.addAnimationView(controller: catsController, animation: animationView )
        animationView.center.x = 0
        let inputPosition = catsController.view.bounds.size.width - animationView.catImageOffset
        let inputNavigationTitle = catsController.navigation.mainTitle
        animationView.transitionOfImageToButton(animationView.catImageView)
        animationView.animateTranslation(catsController)
        if let image = animationView.catImageView {
            XCTAssertTrue(image.superview is UIButton, "Random button should be superview of CatImageView")
            XCTAssertEqual(animationView.center.x, inputPosition)
            XCTAssertEqual(catsController.navigationItem.title, inputNavigationTitle) }
        
    
    }
    
    
    
}
