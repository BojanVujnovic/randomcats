//
//  StoryboardTests.swift
//  RandomCatsTDD
//
//  Created by Mac on 6/3/16.
//  Copyright © 2016 Boki. All rights reserved.
//

import XCTest
@testable import RandomCatsTDD

class StoryboardTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialViewController_IsCatsViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let rootViewController = navigationController.viewControllers[0]
        XCTAssertTrue(rootViewController is CatsViewController)
    }
    
}
